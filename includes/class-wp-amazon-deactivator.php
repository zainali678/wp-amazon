<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://glowlogix.com/
 * @since      1.0.0
 *
 * @package    Wp_Amazon
 * @subpackage Wp_Amazon/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Amazon
 * @subpackage Wp_Amazon/includes
 * @author     Glowlogix <info@glowlogix.com>
 */
class Wp_Amazon_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
