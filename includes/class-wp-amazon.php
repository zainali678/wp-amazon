<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://glowlogix.com/
 * @since      1.0.0
 *
 * @package    Wp_Amazon
 * @subpackage Wp_Amazon/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wp_Amazon
 * @subpackage Wp_Amazon/includes
 * @author     Glowlogix <info@glowlogix.com>
 */
class Wp_Amazon {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wp_Amazon_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		// Hook into the admin menu
    	add_action( 'admin_menu', array( $this, 'create_plugin_settings_page' ) );


        // Add Settings and Fields

		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'wp-amazon';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}
  	public function create_plugin_settings_page() {
    	// Add the menu item and page
    	$page_title = 'Automate Orders';
    	$menu_title = 'Automate-Orders';
    	$capability = 'manage_options';
    	$slug = 'wp-amazon';
    	$callback = array( $this, 'wp_amazon_dashboard');
    	$icon = 'dashicons-admin-plugins';
    	$position = 100;

    	 add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
    	  add_submenu_page('wp-amazon',
        'Products', //page title
        'Products', //menu title
        'edit_themes', //capability,
        'amazon-products',//menu slug
        array( $this, 'wp_amazon_products') //callback function
    );
    	   add_submenu_page('wp-amazon',
        'Order', //page title
        'Order', //menu title
        'edit_themes', //capability,
        'live-order',//menu slug
        array( $this, 'wp_amazon_orders') //callback function
    );
    	    add_submenu_page('wp-amazon',
        'Account Setting', //page title
        'Account Setting', //menu title
        'edit_themes', //capability,
        'account-setting',//menu slug
        array( $this, 'wp_amazon_account_seeting') //callback function
    );

  	}

    public function wp_amazon_dashboard() 
    {?>
    	<div class="wrap">
    		<h2 class="wp-amazon-title"><span class="A">A</span>DASH board</h2><br><br>
    		<div class="__main">
	    		<div class="links">	
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=amazon-products');?>">Products (Search for new products)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=live-order');?>">Order (Live Update)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=account-setting');?>">Account Settings</a></div>
		    	</div>
		    	<div class="snapshot">
		    		<h3>-Sales Snapshot-</h3>
		    		<div class="sales-report">
		    			<div class="main_rep">
			    			<div class="report-product">
					    		<p class="week">Sales This week</p>
					    		<div class="products_report">
						    		<p class="price">$234.95</p>
						    		<p class="item_sold">8 items sold...</p>
					    		</div>
					    		<p class="week">Sales Today</p>
					    		<div class="products_report">
						    		<p class="price">$21</p>
						    		<p class="item_sold">1 item sold so far...</p>
						    	</div>
				    		</div>
				    		<div class="best-selling">
					    		<p class="week">Best selling item</p>
					    		<p>Most sold product image</p>
					    	</div>
				    	</div>
		    		</div>
		    	</div>
	    	</div>
    	</div> <?php
    }
	public function wp_amazon_products() {?>
    	<div class="wrap">
    		<h2 class="wp-amazon-title"><span class="A">A</span>PRODUCT Search</h2><br><br>
    		<div class="__main">
	    		<div class="links">	
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=amazon-products');?>">Products (Search for new products)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=live-order');?>">Order (Live Update)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=account-setting');?>">Account Settings</a></div>
		    	</div>
		    	<div class="snapshot">
		    		<p class="form_search">Product Niche:</p>
		    		<form>
		    			<input class="amazon-product" type="text" name="amazon-product">
		    			<button class="search-product">Research</button>
		    		</form>
		    	</div>
	    	</div>
	    	<div class="amazon_products">
	    		<h2>Showing 10-100</h2>
	    		<div class="list_products">
	    			<div class="prducts">
	    				<div class="dir">
		    				<div class="product-img">
		    					<img src="https://images-na.ssl-images-amazon.com/images/I/910aBMZYqDL._SX679_.jpg">
		    				</div>
		    				<div class="product-desc">
			    				<h2>AmazonBasics Black Soft-Sided Pet Carrier - Medium</h2>
			    				<p>by <a href="#"> AmazonBasics</a></p>
			    				<p>4.3 out of 5 stars 1,633 customer reviews</p>
			    				<p>In Stock.
								This item does not ship to Pakistan. Please check other sellers who may ship internationally. Learn more
								Ships from and sold by Amazon.com in easy-to-open </p>
							</div>
							<div class="compare_price">
			    					<p class="amazon_price">Amazon Price:$15.99</p>
			    					<p>You Sell for:$16.99</p>
			    					<a class="add_product" href="#">Add Product</a>
			    				</div>
						</div>
	    			</div>
		    		<div class="prducts">
		    			<div class="dir">
			    			<div class="product-img">
			    				<img src="https://images-na.ssl-images-amazon.com/images/I/91cxwRA-OAL._SX679_.jpg">
			    				</div>
			    				<div class="product-desc">
				    				<h2>AmazonBasics Black Soft-Sided Pet Carrier - Medium</h2>
				    				<p>by <a href="#"> AmazonBasics</a></p>
				    				<p>4.3 out of 5 stars 1,633 customer reviews</p>
				    				<p>In Stock.
									This item does not ship to Pakistan. Please check other sellers who may ship internationally. Learn more
									Ships from and sold by Amazon.com in easy-to-open </p>
								</div>
								<div class="compare_price">
			    					<p class="amazon_price">Amazon Price:$15.99</p>
			    					<p>You Sell for:$16.99</p>
			    					<a class="add_product" href="#">Add Product</a>
			    				</div>
							</div>
		    			</div>
	    			</div>
    		</div> <?php
    }
	
	public function wp_amazon_orders() {?>
    	<div class="wrap">
    		<h2 class="wp-amazon-title"><span class="A">A</span>LIVE orders</h2><br><br>
    		<div class="main">
	    		<div class="links">	
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=amazon-products');?>">Products (Search for new products)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=live-order');?>">Order (Live Update)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=account-setting');?>">Account Settings</a></div>
		    	</div><br><br>
		    	<div class="orders">
		    		<form>
		    			<p class="date">Search date:</p>
		    			<input class="search_date" type="text" name="date">
		    			<button class="btn_go">Go</button>
		    		</form><br>
					<table class="order_table">
					  <tr>
					    <th>Item Sold</th>
					    <th>Sold Price</th>
					    <th>Profit</th>
					    <th>Order Status</th>
					  </tr>
					  <tr>
					    <td>ID1233 - Double locked basic dog cage: </td>
					    <td>£42.99 </td>
					    <td>£12.99</td>
					    <td>Proccessed</td>
					  </tr>
					  <tr>
					    <td>ID1233 - sleeping basket for dogs:</td>
					    <td>£42.99</td>
					    <td>£12.99</td>
					    <td>ERROR <a href="#"> View Errors</a></td>
					  </tr>
					  <tr>
					    <td>ID1233 - Chew toys for all pets:</td>
					    <td>£42.99</td>
					    <td>£12.99</td>
					    <td>Proccessed</td>
					  </tr>
					  <tr>
					    <td>ID1233 - blue dog basket:</td>
					    <td>£42.99</td>
					    <td>£12.99</td>
					    <td>Proccessed</td>
					  </tr>
					  <tr>
					    <td>ID1233 - Basic doggy bedding:</td>
					    <td>£42.99</td>
					    <td>£12.99</td>
					    <td>Proccessed</td>
					  </tr>
					  <tr>
					    <td>ID1233 - locked basic dog cage:</td>
					    <td>£42.99</td>
					    <td>£12.99</td>
					    <td>Proccessed</td>
					  </tr>
					</table>
		    	</div>
	    	</div>
	    	 <?php
    }

	public function wp_amazon_account_seeting() {?>
    	<div class="wrap">
    		<h2 class="wp-amazon-title"><span class="A">A</span>ACCOUNT settings</h2><br><br>
    		<div class="main">
	    		<div class="links">	
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=amazon-products');?>">Products (Search for new products)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=live-order');?>">Order (Live Update)</a></div>
		    		<div class="dashlink"><a href="<?php echo site_url('/wp-admin/admin.php?page=account-setting');?>">Account Settings</a></div>
		    	</div><br><br>
		    	<div class="accont_div">
		    		<form class="account">
		    			<div>
		    			<label>Amazon Username:</label> <input class="user" type="text" name="user">
		    			</div>
		    			<div>
		    			<label>Amazon Password:</label> <input class="pass" type="text" name="pass">
		    			</div>
		    			<div>
		    			<label>Long Card Number:</label>  <input class="card_num" type="text" name="card_num">
		    			</div>
		    			<div>
		    			<label>ccv Code:</label>  <input class="ccv" type="text" name="ccv">
		    			</div>
		    			<div>
		    			<label>Expiry Date:</label>  <input class="date" type="text" name="date">
		    			</div>
		    			<div>
		    			<label>Name on Card:</label>  <input class="name" type="text" name="name">
		    			</div>
		    			<div>
		    			<label>Profit on Products:</label>  <input class="profit" type="text" name="profit">
		    			</div>
		    			<button class="save">Save</button>
		    		</form>
		    	</div>
	    	</div>
	    	 <?php
    }

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wp_Amazon_Loader. Orchestrates the hooks of the plugin.
	 * - Wp_Amazon_i18n. Defines internationalization functionality.
	 * - Wp_Amazon_Admin. Defines all hooks for the admin area.
	 * - Wp_Amazon_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-amazon-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-amazon-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-amazon-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wp-amazon-public.php';

		$this->loader = new Wp_Amazon_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wp_Amazon_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wp_Amazon_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wp_Amazon_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wp_Amazon_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wp_Amazon_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
