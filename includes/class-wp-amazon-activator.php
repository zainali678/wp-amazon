<?php

/**
 * Fired during plugin activation
 *
 * @link       http://glowlogix.com/
 * @since      1.0.0
 *
 * @package    Wp_Amazon
 * @subpackage Wp_Amazon/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Amazon
 * @subpackage Wp_Amazon/includes
 * @author     Glowlogix <info@glowlogix.com>
 */
class Wp_Amazon_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
